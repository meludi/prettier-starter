# Prettier Starter

### A prettier lint starterkit

## Version

- 0.0.1

## Requirements

- node >=8.x
- npm >=6.x

## Quick start

### Install

- **Note: requires a node version >= 8 and an npm version >= 6.**

Activate node version.

```sh
# switch to node version 8.3.0
$ nvm use
```

Install dependencies.

```sh
# install dependencies
$ npm install
or
$ yarn
```

### NPM Scripts

```sh
# eslint
$ npm run eslint
$ npm run eslint:fix

# stylelint
$ npm run stylelint
$ npm run stylelint:fix

# precommit (linitin git stage)
$ npm run precommit
```

## Editor Configuration

### VSCode

- [Editorconfig](https://github.com/editorconfig/editorconfig-vscode)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [ESLint](https://github.com/Microsoft/vscode-eslint)
- [Stylelint](https://github.com/shinnn/vscode-stylelint)

## License

- GPL-2.0+

**copyright maa**
